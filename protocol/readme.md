# Pure scripts implementation :

## in Perl:

https://github.com/avar/sendmail-pmilter 

## in python:

https://github.com/crustymonkey/python-libmilter

https://github.com/humantech/yatxmilter

## protocol description
[Protocol specification v2](milter-protocol.txt)
